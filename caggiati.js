const resizeLogo = () => {
    if(window.innerWidth <= 480) {
        document.getElementById('logoPrincipalLargoBlanco').style.maxWidth = '50%';
        document.getElementById('logoPrincipalLargoNegro').style.maxWidth = '50%';
    }
    if(window.innerWidth > 480 && window.innerWidth <= 768 ) {
        document.getElementById('logoPrincipalLargoBlanco').style.maxWidth = '70%';
        document.getElementById('logoPrincipalLargoNegro').style.maxWidth = '70%';
    }
    if(window.innerWidth > 768 ) {
        document.getElementById('logoPrincipalLargoBlanco').style.maxWidth = '100%';
        document.getElementById('logoPrincipalLargoNegro').style.maxWidth = '100%';
    }
}

resizeLogo();

window.onresize = () => { resizeLogo(); }